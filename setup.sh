
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/atlas-online-nightlies.cern.ch/felix/releases/felix-04-02-01-rm5-stand-alone/x86_64-centos7-gcc11-opt/lib

alias cm_setup='source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh'

# cm_setup tdaq-09-04-00 x86_64-centos7-gcc11-opt
cm_setup tdaq-09-05-00 x86_64-centos7-gcc11-opt
