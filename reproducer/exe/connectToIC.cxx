
#include <cstdio>
#include <iostream>
#include <unistd.h>

#include <felix/felix_client_properties.h>
#include <felix/felix_client_thread_interface.hpp>
#include <felix_proxy/ClientThread.h>
// #include "felix/felix_client_thread.hpp" // FIXME how do I get this in?

void on_init() { printf("[UserClass] on_init called\n"); }

void on_connect(uint64_t fid) {
  printf("[UserClass] on_connect called 0x%lx\n", fid);
}

void on_disconnect(uint64_t fid) {
  printf("[UserClass] on_disconnect called 0x%lx\n", fid);
}

void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status) {
  printf("[UserClass] on_data called 0x%lx with size %lu\n", fid, size);
}

int main(int ac, char* av[]) {

  std::cout << "Hello!\n";

  FelixClientThreadInterface::Config config;
  // FelixClientThread::Config config; // FIXME should be this??

  config.on_init_callback = std::bind(&on_init);
  config.on_data_callback =
      std::bind(&on_data, std::placeholders::_1, std::placeholders::_2,
                std::placeholders::_3, std::placeholders::_4);
  config.on_connect_callback = std::bind(&on_connect, std::placeholders::_1);
  config.on_disconnect_callback =
      std::bind(&on_disconnect, std::placeholders::_1);

  config.property[FELIX_CLIENT_LOCAL_IP_OR_INTERFACE] = "130.237.34.147";
  config.property[FELIX_CLIENT_LOG_LEVEL] = "info";
  config.property[FELIX_CLIENT_BUS_DIR] = "/home/aleopold/sandbox/bus";
  config.property[FELIX_CLIENT_BUS_GROUP_NAME] = "FELIX";
  config.property[FELIX_CLIENT_VERBOSE_BUS] = "True";
  config.property[FELIX_CLIENT_TIMEOUT] = "0";

  felix_proxy::ClientThread* client = new felix_proxy::ClientThread(config);
  // FelixClientThread* client = new FelixClientThread(config); // FIXME should
  // be this?

  // receive from IC channel on link 0
  // client->subscribe(std::stoi(av[1]));
  client->subscribe(0x10000000001d0000);

  // send data to IC channel on link 0
  std::vector<uint8_t> data = {0xe1, 0x1, 0x1, 0x0, 0x50, 0x1, 0xb0};

  client->send_data(0x1000000000118000, (const uint8_t*)data.data(),
                    data.size() + 1, true);

  while (true) {
    // wait if some data arrives...
    usleep(5);
  }
}
