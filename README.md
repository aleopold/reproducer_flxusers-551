## Setup
To build, do
```
source setup.sh
source compile.sh
```

after changes to the source code,
```
source recompile.sh
```
is enough.

## Running
To test this, I run in two different terminals

```
felix-toflx --bus-dir ./bus --ip 130.237.34.147 --stats-out=stats --free-cmem
```
and
```
felix-tohost --bus-dir ./bus --ip 130.237.34.147 --stats-out=stats --free-cmem
```

In a third terminal (the one where I setup and build) I execute the user code

```
[aleopold@lumino reproducer_FLXUSERS-551]$ connectToIC
Hello!
2022-11-11 15-02-46.266 [Info] (FelixClient) Local IP: 130.237.34.147
2022-11-11 15-02-46.266 [Info] (FelixClient) Initializing netio-next
2022-11-11 15-02-46.266 [Info] (FelixClient) Setting up re-connect timer 1000 ms
2022-11-11 15-02-46.266 [Info] (FelixClient) Initializing felix-bus
2022-11-11 15-02-46.266 [Info] (callback_on_init) Registering on_init
2022-11-11 15-02-46.266 [Info] (callback_on_data) Registering on_data
2022-11-11 15-02-46.266 [Info] (callback_on_connect) Registering on_connect
2022-11-11 15-02-46.266 [Info] (callback_on_disconnect) Registering on_disconnect
2022-11-11 15-02-46.266 [Info] (run) In run: 7f5958a9c700
2022-11-11 15-02-46.266 [Info] (run) Running netio_run
2022-11-11 15-02-46.266 [Info] (on_init_eventloop) On init
[UserClass] on_init called
2022-Nov-11 15:02:46,366 LOG [felix_proxy::ClientThread::ClientThread(...) at /builds/atlas-tdaq-software/tdaq-cmake/tdaq/tdaq-09-05-00/felix_proxy/src/ClientThread.cpp:55] FelixClient implementation (extended = 1) has been loaded from /cvmfs/atlas-online-nightlies.cern.ch/felix/releases/felix-04-02-01-rm5-stand-alone/x86_64-centos7-gcc11-opt/lib/libfelix-client-lib.so library
Getting info for 0x10000000001d0000
Looking for /home/aleopold/sandbox/bus/FELIX/0/0
Looking for 0x10000000001d0000 in "/home/aleopold/sandbox/bus/FELIX/0/0/dma-4.ndjson"
Stale file "/home/aleopold/sandbox/bus/FELIX/0/0/dma-4.ndjson" (ignored)
Looking for 0x10000000001d0000 in "/home/aleopold/sandbox/bus/FELIX/0/0/dma-0.ndjson"
Stale file "/home/aleopold/sandbox/bus/FELIX/0/0/dma-0.ndjson" (ignored)
0x10000000001d0000: {"hfid":"0x10000000001d0000","fid":1152921504608747520,"ip":"130.237.34.147","port":53500,"unbuffered":false,"pubsub":true,"raw_tcp":false,"netio_pages":512,"netio_pagesize":1024,"host":"lumino.particle.kth.se","pid":2582,"user":"aleopold"}
Getting info for 0x1000000000118000
Looking for /home/aleopold/sandbox/bus/FELIX/0/0
Looking for 0x1000000000118000 in "/home/aleopold/sandbox/bus/FELIX/0/0/dma-4.ndjson"
Stale file "/home/aleopold/sandbox/bus/FELIX/0/0/dma-4.ndjson" (ignored)
0x1000000000118000: {"hfid":"0x1000000000118000","fid":1152921504607993856,"ip":"130.237.34.147","port":53200,"unbuffered":false,"pubsub":false,"raw_tcp":false,"netio_pages":256,"netio_pagesize":65536,"host":"lumino.particle.kth.se","pid":2562,"user":"aleopold"}
Getting info for 0x10000000001d0000
2022-11-11 15-02-46.415 [Notice] (send_data) Address 130.237.34.147:53200
Looking for /home/aleopold/sandbox/bus/FELIX/0/0
2022-11-11 15-02-46.415 [Info] (send_data) Not eventloop thread! instead: 7f595a8ae740, event thread is 7f5958a9c700 FID: 1000000000118000
Looking for 0x10000000001d0000 in "/home/aleopold/sandbox/bus/FELIX/0/0/dma-4.ndjson"
Stale file "/home/aleopold/sandbox/bus/FELIX/0/0/dma-4.ndjson" (ignored)
Looking for 0x10000000001d0000 in "/home/aleopold/sandbox/bus/FELIX/0/0/dma-0.ndjson"
Stale file "/home/aleopold/sandbox/bus/FELIX/0/0/dma-0.ndjson" (ignored)
0x10000000001d0000: {"hfid":"0x10000000001d0000","fid":1152921504608747520,"ip":"130.237.34.147","port":53500,"unbuffered":false,"pubsub":true,"raw_tcp":false,"netio_pages":512,"netio_pagesize":1024,"host":"lumino.particle.kth.se","pid":2582,"user":"aleopold"}
Getting info for 0x1000000000118000
Looking for /home/aleopold/sandbox/bus/FELIX/0/0
Looking for 0x1000000000118000 in "/home/aleopold/sandbox/bus/FELIX/0/0/dma-4.ndjson"
Stale file "/home/aleopold/sandbox/bus/FELIX/0/0/dma-4.ndjson" (ignored)
0x1000000000118000: {"hfid":"0x1000000000118000","fid":1152921504607993856,"ip":"130.237.34.147","port":53200,"unbuffered":false,"pubsub":false,"raw_tcp":false,"netio_pages":256,"netio_pagesize":65536,"host":"lumino.particle.kth.se","pid":2562,"user":"aleopold"}
2022-11-11 15-02-47.866 [Notice] (send_data) Address 130.237.34.147:53200
2022-11-11 15-02-47.866 [Info] (get_buffered_send_socket) Setup buffered send socket
2022-11-11 15-02-47.873 [Info] (on_connection_established) Buffered send connection established
2022-11-11 15-02-47.873 [Info] (on_connection_established) on_connection_established
[UserClass] on_connect called 0x1000000000118000
2022-11-11 15-02-47.892 [Notice] (send_data) Sending (buffered) for 1152921504607993856 i.e. tag 17
2022-11-11 15-02-47.892 [Notice] (send_data) Status 0
```

_It seems to send, but the connection to the "returning" elink (0x10000000001d0000) apparently can't be made_

_I see a_
```
2022-11-11 15:02:47 ERRORnetio pubsub.c:285: Cannot unsubscribe tag 0x1152921504608747520. Socket not found!
```
_in the `felix-tohost` output_
